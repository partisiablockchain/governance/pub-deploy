# Pub deploy

System contract for deploying public WASM contracts.

**This contract does not support deployment of ZK/REAL WASM contracts.**

This is a core contract for Partisia Blockchain, as it allows any user to deploy a new smart
contract, using one of the stored binders. The deploy contract keeps track of multiple binders to
allow for a grace period before binders are fully deprecated.

[Public WASM Binder](https://gitlab.com/partisiablockchain/language/pub-wasm-binder) is the primary binder
that contracts will be deployed with.

## Further links

- [Javadoc Documentation for `main` branch](https://partisiablockchain.gitlab.io/governance/pub-deploy)
