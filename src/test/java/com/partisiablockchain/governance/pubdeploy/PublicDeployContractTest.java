package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.ContractEventDeploy;
import com.partisiablockchain.contract.sys.ContractEventUpgrade;
import com.partisiablockchain.contract.sys.Deploy;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.Upgrade;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests of {@link PublicDeployContract}. */
public final class PublicDeployContractTest {

  private static final BlockchainAddress ACCOUNT_FROM =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  private final SysContractContextTest context = new SysContractContextTest(0, 100, ACCOUNT_FROM);

  private final SysContractSerialization<PublicDeployContractState> serialization =
      new SysContractSerialization<>(
          PublicDeployContractInvoker.class, PublicDeployContractState.class);

  private final byte[] jar = new byte[23];
  private final byte[] binder = new byte[20];

  private final SemanticVersion supportedBinderVersionMin = new SemanticVersion(7, 0, 0);
  private final SemanticVersion supportedBinderVersionMax = new SemanticVersion(9, 2, 1);
  private final byte[] abi = new byte[52];
  private final BlockchainAddress systemUpdateAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");
  private final PublicDeployContractState state =
      createContractState(
          binder, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);
  private final byte[] initialization = {1};
  private BlockchainAddress blockchainAddress;

  /** Setup default contract and event manager. */
  @BeforeEach
  public void setUp() {
    blockchainAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC, context.getOriginalTransactionHash());
  }

  /** A state should have the correct initial values, binder info and system update address. */
  @Test
  public void deploy() {
    PublicDeployContractState createdState =
        serialization.create(
            context,
            out -> {
              out.writeDynamicBytes(jar);
              supportedBinderVersionMin.write(out);
              supportedBinderVersionMax.write(out);
              systemUpdateAddress.write(out);
            });

    int binderId = createdState.getNextBinderId() - 1;
    assertThat(createdState.getBinderInfo(binderId).bindingJar().getData()).isEqualTo(jar);
    assertThat(
            createdState
                .getBinderInfo(binderId)
                .versionInterval()
                .supportedBinderVersionMin()
                .equals(supportedBinderVersionMin))
        .isTrue();
    assertThat(
            createdState
                .getBinderInfo(binderId)
                .versionInterval()
                .supportedBinderVersionMax()
                .equals(supportedBinderVersionMax))
        .isTrue();
    assertThat(createdState.getSystemUpdateAddress()).isEqualTo(systemUpdateAddress);
  }

  /** When deploying a contract, the contract should be deployed correctly on the blockchain. */
  @Test
  public void deployContract() {
    PublicDeployContractState invokedState =
        serialization.invoke(
            context,
            state,
            out -> {
              out.writeByte(PublicDeployContract.Invocations.DEPLOY_CONTRACT);
              out.writeDynamicBytes(jar);
              out.writeDynamicBytes(abi);
              out.writeDynamicBytes(initialization);
            });
    assertInvokeState(invokedState);
    assertDeploy(null);
  }

  /**
   * When deploying a contract with binder id, the contract should be deployed correctly on the
   * blockchain, choosing the binder determined by the binder id.
   */
  @Test
  public void deployContractWithBinderId() {
    int binderId = 1;
    PublicDeployContractState invokedState =
        serialization.invoke(
            context,
            state,
            out -> {
              out.writeByte(PublicDeployContract.Invocations.DEPLOY_CONTRACT_WITH_BINDER_ID);
              out.writeDynamicBytes(jar);
              out.writeDynamicBytes(abi);
              out.writeDynamicBytes(initialization);
              out.writeInt(binderId);
            });
    assertInvokeState(invokedState);
    assertDeploy(null);
  }

  /**
   * When deploying a contract with a binder that has been saved in the storage plugin, this
   * contract should deploy on the blockchain using the binder hash instead of the full binder jar.
   */
  @Test
  public void deployContractWithBinderHash() {
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);
    PublicDeployContract contract = new PublicDeployContract();
    SemanticVersion newSupportedBinderVersionMin = new SemanticVersion(9, 3, 0);
    SemanticVersion newSupportedBinderVersionMax = new SemanticVersion(10, 2, 1);
    byte[] newJar = new byte[23];

    state =
        contract.addBinder(
            context, state, newJar, newSupportedBinderVersionMin, newSupportedBinderVersionMax);
    Hash binderHash = new LargeByteArray(newJar).getIdentifier();
    state =
        saveBinderCallback(
            contract,
            state,
            newJar,
            binderHash,
            newSupportedBinderVersionMin,
            newSupportedBinderVersionMax);

    int id = state.getNextBinderId() - 1;
    contract.deployContractWithBinderId(context, state, jar, abi, initialization, id);

    assertDeploy(binderHash);
  }

  private PublicDeployContractState saveBinderCallback(
      PublicDeployContract contract,
      PublicDeployContractState state,
      byte[] newJar,
      Hash binderHash,
      SemanticVersion newSupportedBinderVersionMin,
      SemanticVersion newSupportedBinderVersionMax) {
    assertSaveBinderRemoteCall(newJar, newSupportedBinderVersionMin, newSupportedBinderVersionMax);
    CallbackContext callbackContext = createSaveBinderCallbackContext(true);
    return contract.saveBinderCallback(
        context,
        state,
        callbackContext,
        binderHash,
        newSupportedBinderVersionMin,
        newSupportedBinderVersionMax);
  }

  private void assertInvokeState(PublicDeployContractState state) {
    int binderId = state.getNextBinderId() - 1;
    assertThat(state).isNotNull();
    assertThat(state.getBinderInfo(binderId).bindingJar().getData())
        .isEqualTo(state.getBinderInfo(binderId).bindingJar().getData());
    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMin())
        .isEqualTo(supportedBinderVersionMin);
    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMax())
        .isEqualTo(supportedBinderVersionMax);
    assertThat(state.getSystemUpdateAddress()).isEqualTo(systemUpdateAddress);
  }

  /** Assert that a deployment transaction succeeded, and deployed the correct binder. */
  private void assertDeploy(Hash binderHash) {
    List<ContractEvent> eventGroup = context.getInteractions();
    ContractEventDeploy deploymentEvent = (ContractEventDeploy) eventGroup.get(0);
    Deploy deploy = deploymentEvent.deploy;
    assertThat(deploy.contract).isEqualTo(blockchainAddress);
    if (binderHash == null) {
      assertThat(deploy.binderJar).isEqualTo(binder);
    } else {
      assertThat(deploy.binderJar).isEqualTo(binderHash.getBytes());
    }
    assertThat(deploy.contractJar).isEqualTo(jar);
    assertThat(SafeDataOutputStream.serialize(deploy.rpc)).isEqualTo(initialization);
    assertThat(deploymentEvent.allocatedCost).isEqualTo(null);
  }

  /**
   * A contract of the current contract version (i.e. contains a system update contract address)
   * should be able to be upgraded correctly, preserving the state's fields.
   */
  @Test
  public void upgradeDeployContractFromCurrent() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);

    state = contract.upgrade(StateAccessor.create(state), systemUpdateAddress);

    int binderId = state.getNextBinderId() - 1;

    // Check that the new state has same system update address and has both binders
    assertThat(state.getBinderInfo(binderId).bindingJar().getData())
        .isEqualTo(state.getBinderInfo(binderId).bindingJar().getData());
    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMin())
        .isEqualTo(supportedBinderVersionMin);
    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMax())
        .isEqualTo(supportedBinderVersionMax);
    assertThat(state.getSystemUpdateAddress()).isEqualTo(systemUpdateAddress);
  }

  /**
   * When upgrading a contract with multiple binders, all binders should be preserved and keep their
   * unique ID.
   */
  @Test
  public void upgradeDeployContractWithMultipleBinders() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);

    SemanticVersion newSupportedBinderVersionMin = new SemanticVersion(9, 3, 0);
    SemanticVersion newSupportedBinderVersionMax = new SemanticVersion(10, 2, 1);
    byte[] newJar = new byte[23];
    newJar[1] = 5;

    final int binderId = state.getNextBinderId() - 1;

    // Add a new binder to the state
    state =
        contract.addBinder(
            context, state, newJar, newSupportedBinderVersionMin, newSupportedBinderVersionMax);
    Hash binderHash = new LargeByteArray(newJar).getIdentifier();
    state =
        saveBinderCallback(
            contract,
            state,
            newJar,
            binderHash,
            newSupportedBinderVersionMin,
            newSupportedBinderVersionMax);

    int binderIdBeforeUpgrade = state.getNextBinderId() - 1;

    state = contract.upgrade(StateAccessor.create(state), systemUpdateAddress);

    int binderIdAfterUpgrade = state.getNextBinderId() - 1;

    // The nextBinderId value has been preserved
    assertThat(binderIdBeforeUpgrade).isEqualTo(binderIdAfterUpgrade);

    // Check that the new state has the first binder
    assertThat(state.getBinderInfo(binderId).bindingJar().getData())
        .isEqualTo(state.getBinderInfo(binderId).bindingJar().getData());
    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMin())
        .isEqualTo(supportedBinderVersionMin);

    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMax())
        .isEqualTo(supportedBinderVersionMax);

    // Check that the new state has the new binder
    assertThat(state.getBinderInfo(binderIdAfterUpgrade).bindingJar().getData())
        .isEqualTo(binderHash.getBytes());

    assertThat(
            state.getBinderInfo(binderIdAfterUpgrade).versionInterval().supportedBinderVersionMin())
        .isEqualTo(newSupportedBinderVersionMin);

    assertThat(
            state.getBinderInfo(binderIdAfterUpgrade).versionInterval().supportedBinderVersionMax())
        .isEqualTo(newSupportedBinderVersionMax);
  }

  /**
   * When a system update contract address adds a binder, the binder (BinderInfo) should be added to
   * and accessible in the state.
   */
  @Test
  public void addBinder() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);

    SemanticVersion newSupportedBinderVersionMin = new SemanticVersion(9, 3, 0);
    SemanticVersion newSupportedBinderVersionMax = new SemanticVersion(10, 2, 1);
    byte[] newJar = new byte[23];

    state =
        contract.addBinder(
            context, state, newJar, newSupportedBinderVersionMin, newSupportedBinderVersionMax);
    Hash binderHash = new LargeByteArray(newJar).getIdentifier();
    state =
        saveBinderCallback(
            contract,
            state,
            newJar,
            binderHash,
            newSupportedBinderVersionMin,
            newSupportedBinderVersionMax);

    int binderId = state.getNextBinderId() - 1;

    // Check that the sender is a system update address
    assertThat(state.getSystemUpdateAddress()).isEqualTo(context.getFrom());
    // Check that the newJar can be accessed with its new ID
    assertThat(state.getBinderInfo(binderId).bindingJar().getData())
        .isEqualTo(binderHash.getBytes());

    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMin())
        .isEqualTo(newSupportedBinderVersionMin);

    assertThat(state.getBinderInfo(binderId).versionInterval().supportedBinderVersionMax())
        .isEqualTo(newSupportedBinderVersionMax);
  }

  /**
   * When a system update contract address adds multiple binders, they should be stored with
   * different IDs in the state.
   */
  @Test
  public void addMultipleBinders() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);

    SemanticVersion supportedBinderVersionMin1 = new SemanticVersion(9, 3, 1);
    SemanticVersion supportedBinderVersionMax1 = new SemanticVersion(10, 2, 1);
    byte[] newJar1 = new byte[23];
    newJar1[22] = 1;

    final int binderId1 = state.getNextBinderId();

    state =
        contract.addBinder(
            context, state, newJar1, supportedBinderVersionMin1, supportedBinderVersionMax1);
    Hash binderHash1 = new LargeByteArray(newJar1).getIdentifier();
    state =
        saveBinderCallback(
            contract,
            state,
            newJar1,
            binderHash1,
            supportedBinderVersionMin1,
            supportedBinderVersionMax1);

    SemanticVersion supportedBinderVersionMin2 = new SemanticVersion(9, 3, 9);
    SemanticVersion supportedBinderVersionMax2 = new SemanticVersion(10, 5, 1);
    byte[] newJar2 = new byte[23];
    newJar2[22] = 2;

    final int binderId2 = state.getNextBinderId();

    state =
        contract.addBinder(
            context, state, newJar2, supportedBinderVersionMin2, supportedBinderVersionMax2);
    Hash binderHash2 = new LargeByteArray(newJar2).getIdentifier();
    state =
        saveBinderCallback(
            contract,
            state,
            newJar2,
            binderHash2,
            supportedBinderVersionMin2,
            supportedBinderVersionMax2);

    // Check that the sender is a system update address
    assertThat(state.getSystemUpdateAddress()).isEqualTo(context.getFrom());
    // Check that the new binders can be accessed
    assertThat(state.getBinderInfo(binderId1).bindingJar().getData())
        .isEqualTo(binderHash1.getBytes());
    assertThat(state.getBinderInfo(binderId1).versionInterval().supportedBinderVersionMin())
        .isEqualTo(supportedBinderVersionMin1);

    assertThat(state.getBinderInfo(binderId1).versionInterval().supportedBinderVersionMax())
        .isEqualTo(supportedBinderVersionMax1);
    assertThat(state.getBinderInfo(binderId2).bindingJar().getData())
        .isEqualTo(binderHash2.getBytes());
    assertThat(state.getBinderInfo(binderId2).versionInterval().supportedBinderVersionMin())
        .isEqualTo(supportedBinderVersionMin2);

    assertThat(state.getBinderInfo(binderId2).versionInterval().supportedBinderVersionMax())
        .isEqualTo(supportedBinderVersionMax2);
  }

  /** When the call to add a binder does not come from a system update address, the call fails. */
  @Test
  public void addBinderNotSystemUpdateAddress() {
    PublicDeployContract contract = new PublicDeployContract();
    BlockchainAddress newBlockchainAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000005");
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, newBlockchainAddress);

    SemanticVersion newSupportedBinderVersionMin = new SemanticVersion(9, 3, 0);
    SemanticVersion newSupportedBinderVersionMax = new SemanticVersion(10, 2, 1);
    byte[] newJar = new byte[23];

    // Check that the sender is not a system update address
    assertThat(state.getSystemUpdateAddress()).isNotEqualTo(context.getFrom());

    assertThatThrownBy(
            () ->
                contract.addBinder(
                    context,
                    state,
                    newJar,
                    newSupportedBinderVersionMin,
                    newSupportedBinderVersionMax))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("The invocation does not come from a system update contract.");
  }

  /**
   * When a system update contract address removes a binder, the binder (BinderInfo) should be
   * removed from the state.
   */
  @Test
  public void removeBinder() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            binder, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);
    int binderId = state.getNextBinderId() - 1;
    // Check that the new jar can be accessed with its new id
    assertThat(state.getBinderInfo(binderId).bindingJar().getData()).isEqualTo(binder);
    // Sender calls removeBinder on binder with id 1
    PublicDeployContractState removeState = contract.removeBinder(context, state, binderId);

    assertThat(removeState.getBinderInfo(binderId)).isNull();
  }

  /**
   * When a system update contract address removes a binder which has already been removed, the
   * binder remains inaccessible.
   */
  @Test
  public void removeBinderTwice() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            binder, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);
    int binderId = state.getNextBinderId() - 1;
    // Check that the new jar can be accessed with the id
    assertThat(state.getBinderInfo(binderId).bindingJar().getData()).isEqualTo(binder);
    // Sender calls removeBinder on binder with id 1
    PublicDeployContractState removeState = contract.removeBinder(context, state, binderId);
    assertThat(removeState.getBinderInfo(binderId)).isNull();
    PublicDeployContractState removeTwiceState = contract.removeBinder(context, state, binderId);
    assertThat(removeTwiceState.getBinderInfo(binderId)).isNull();
  }

  /**
   * When a system update contract address removes and adds several binders, the state allows
   * "holes" in the binder IDs, hence preserving uniqueness of the binder IDs.
   */
  @Test
  public void removeAndAddMultipleBinders() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);

    SemanticVersion supportedBinderVersionMin1 = new SemanticVersion(9, 3, 0);
    SemanticVersion supportedBinderVersionMax1 = new SemanticVersion(10, 2, 1);
    byte[] newJar1 = new byte[23];
    newJar1[22] = 1;

    final int binderId1 = state.getNextBinderId();

    state =
        contract.addBinder(
            context, state, newJar1, supportedBinderVersionMin1, supportedBinderVersionMax1);
    Hash binderHash1 = new LargeByteArray(newJar1).getIdentifier();
    state =
        saveBinderCallback(
            contract,
            state,
            newJar1,
            binderHash1,
            supportedBinderVersionMin1,
            supportedBinderVersionMax1);

    state = contract.removeBinder(context, state, binderId1);
    final int binderId2 = state.getNextBinderId();

    SemanticVersion supportedBinderVersionMin2 = new SemanticVersion(9, 3, 9);
    SemanticVersion supportedBinderVersionMax2 = new SemanticVersion(10, 5, 1);
    byte[] newJar2 = new byte[23];
    newJar2[22] = 2;

    state =
        contract.addBinder(
            context, state, newJar2, supportedBinderVersionMin2, supportedBinderVersionMax2);
    Hash binderHash2 = new LargeByteArray(newJar2).getIdentifier();
    assertSaveBinderRemoteCall(newJar2, supportedBinderVersionMin2, supportedBinderVersionMax2);
    state =
        saveBinderCallback(
            contract,
            state,
            newJar2,
            binderHash2,
            supportedBinderVersionMin2,
            supportedBinderVersionMax2);

    // Check that the binder we removed has been removed and the state's next binder ID has been
    // incremented.
    assertThat(state.getBinderInfo(binderId1)).isNull();
    assertThat(binderId2).isEqualTo(binderId1 + 1);

    assertThat(state.getBinderInfo(binderId2).bindingJar().getData())
        .isEqualTo(binderHash2.getBytes());
    assertThat(state.getBinderInfo(binderId2).versionInterval().supportedBinderVersionMin())
        .isEqualTo(supportedBinderVersionMin2);

    assertThat(state.getBinderInfo(binderId2).versionInterval().supportedBinderVersionMax())
        .isEqualTo(supportedBinderVersionMax2);
  }

  /**
   * When the call to remove a binder does not come from a system update address, the call fails.
   */
  @Test
  public void removeBinderNotVotingAddress() {
    // First, we add a new binder with id 3
    PublicDeployContract contract = new PublicDeployContract();
    BlockchainAddress newBlockchainAddress =
        BlockchainAddress.fromString("000000000000000000000000000000000000000005");
    PublicDeployContractState state =
        createContractState(
            binder, supportedBinderVersionMin, supportedBinderVersionMax, newBlockchainAddress);
    // Check that the binder can be accessed with id 1
    int binderId = state.getNextBinderId() - 1;
    assertThat(state.getBinderInfo(binderId).bindingJar().getData()).isEqualTo(binder);

    // Check that the sender is not a voting address
    assertThat(state.getSystemUpdateAddress()).isNotEqualTo(context.getFrom());

    assertThatThrownBy(() -> contract.removeBinder(context, state, binderId))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("The invocation does not come from a system update contract.");
  }

  /** Deployed WASM contracts can be upgraded to a new binder. */
  @Test
  public void upgradeDeployedContract() {
    PublicDeployContract contract = new PublicDeployContract();

    int newBinderId = state.getNextBinderId() - 1;
    byte[] rpc = new byte[33];

    PublicDeployContractState newState =
        contract.upgradeContract(context, state, ACCOUNT_FROM, newBinderId, jar, abi, rpc);
    assertThat(newState).isEqualTo(state);

    ContractEventUpgrade event = (ContractEventUpgrade) context.getInteractions().get(0);
    Upgrade upgrade = event.upgrade;
    assertThat(upgrade.contract).isEqualTo(ACCOUNT_FROM);
    assertThat(upgrade.newContractJar).hasSize(23);
    assertThat(upgrade.newAbi).hasSize(52);

    byte[] upgradeRpc = SafeDataOutputStream.serialize(upgrade.rpc::write);
    assertThat(rpc).isEqualTo(upgradeRpc);
  }

  @Test
  void saveBinderCallbackFailure() {
    PublicDeployContract contract = new PublicDeployContract();
    PublicDeployContractState state =
        createContractState(
            jar, supportedBinderVersionMin, supportedBinderVersionMax, systemUpdateAddress);
    CallbackContext callbackContext = createSaveBinderCallbackContext(false);
    assertThatThrownBy(
            () -> contract.saveBinderCallback(context, state, callbackContext, null, null, null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Failed to save new binder in storage plugin");
  }

  PublicDeployContractState createContractState(
      byte[] bindingJar,
      SemanticVersion supportedBinderVersionMin,
      SemanticVersion supportedBinderVersionMax,
      BlockchainAddress blockchainVotingAddress) {

    SupportedBinderVersionInterval binderInterval =
        new SupportedBinderVersionInterval(supportedBinderVersionMin, supportedBinderVersionMax);

    int binderId = 1;
    AvlTree<Integer, BinderInfo> binders =
        AvlTree.create(
            Map.of(binderId, new BinderInfo(new LargeByteArray(bindingJar), binderInterval)));
    return new PublicDeployContractState(blockchainVotingAddress, binders, binderId + 1);
  }

  private CallbackContext createSaveBinderCallbackContext(boolean success) {
    return CallbackContext.create(
        FixedList.create(
            List.of(
                CallbackContext.createResult(
                    null, success, SafeDataInputStream.createFromBytes(new byte[0])))));
  }

  private void assertSaveBinderRemoteCall(byte[] binder, SemanticVersion min, SemanticVersion max) {
    GlobalPluginStateUpdate expectedUpdateState =
        SharedObjectStorePluginRpc.addSharedObject(binder);
    byte[] expectedCallback =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(PublicDeployContract.Callbacks.SAVE_BINDER_CALLBACK);
              new LargeByteArray(binder).getIdentifier().write(stream);
              min.write(stream);
              max.write(stream);
            });

    GlobalPluginStateUpdate updateState = context.getUpdateGlobalSharedObjectStorePluginState();
    assertThat(updateState).isNotNull();
    assertThat(updateState.getRpc()).isEqualTo(expectedUpdateState.getRpc());
    ContractEventGroup remoteCalls = context.getRemoteCalls();
    assertThat(remoteCalls.callbackRpc).isEqualTo(expectedCallback);
  }
}
