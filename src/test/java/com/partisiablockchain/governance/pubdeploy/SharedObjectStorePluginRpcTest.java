package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.secata.stream.SafeDataInputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests of {@link SharedObjectStorePluginRpc}. */
final class SharedObjectStorePluginRpcTest {

  /**
   * {@link SharedObjectStorePluginRpc#addSharedObject} produces an well-formed invocation to the
   * object storage plugin.
   */
  @Test
  void addSharedObject() {
    byte[] objectBytes = {(byte) 0xAB, (byte) 0xBA, 0x19, 0x72};

    GlobalPluginStateUpdate updateState = SharedObjectStorePluginRpc.addSharedObject(objectBytes);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(updateState.getRpc());
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(SharedObjectStorePluginRpc.ADD_SHARED_OBJECT_INVOCATION_BYTE);
    Assertions.assertThat(stream.readDynamicBytes()).isEqualTo(objectBytes);
  }
}
