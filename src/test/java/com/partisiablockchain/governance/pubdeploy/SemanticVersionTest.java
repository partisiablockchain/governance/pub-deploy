package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for protocol version. */
public final class SemanticVersionTest {

  private static final SemanticVersion EXAMPLE_VERSION = new SemanticVersion(0x35, 0x42, 0x73);

  @Test
  void testHashCode() {
    Assertions.assertThat(EXAMPLE_VERSION.hashCode()).isEqualTo(0x35042073);
  }

  @Test
  void testToString() {
    Assertions.assertThat(EXAMPLE_VERSION.toString()).isEqualTo("53.66.115");
  }

  @Test
  void testEquals() {
    EqualsVerifier.forClass(SemanticVersion.class)
        .withNonnullFields("major", "minor", "patch")
        .verify();
  }
}
