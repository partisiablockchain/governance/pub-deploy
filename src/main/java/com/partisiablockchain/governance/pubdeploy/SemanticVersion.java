package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.SafeDataOutputStream;

/**
 * Version structures with field semantics. Semantic versions are used to describe the compatibility
 * between different versions of components that are updated independently.
 *
 * <p>Generally structured as a host environment providing a version {@code X.Y.Z}, while the client
 * program requires a specific version {@code A.B.C}.
 *
 * <p>The host version {@code X.Y.Z} supports versions {@code X.B.C}, for {@code Y >= B} and any
 * {@code Z}, {@code C}. That is:
 *
 * <ul>
 *   <li>Major version bump: Backwards incompatible change. An old client cannot run in the new host
 *       environment.
 *   <li>Minor version bump: Forwards incompatible change. A new client running on an old host could
 *       use functionality that the host doesn't know about, thus requiring a minor version bump.
 *   <li>Patch version bump: Both backwards and forwards compatible changes. Anything goes.
 * </ul>
 */
@Immutable
public record SemanticVersion(int major, int minor, int patch) implements StateSerializable {

  @Override
  public boolean equals(Object uncheckedOther) {
    return uncheckedOther instanceof SemanticVersion other
        && other.major == this.major
        && other.minor == this.minor
        && other.patch == this.patch;
  }

  @Override
  public int hashCode() {
    return ((this.major & 0xFF) << 24) | ((this.minor & 0xFFF) << 12) | (this.patch & 0xFFF);
  }

  @Override
  public String toString() {
    return String.format("%d.%d.%d", major, minor, patch);
  }

  /**
   * Writes semantic version to output stream.
   *
   * @param stream Stream to write to
   */
  public void write(final SafeDataOutputStream stream) {
    stream.writeInt(major());
    stream.writeInt(minor());
    stream.writeInt(patch());
  }

  /**
   * Creates an instance of a SemanticVersion from a state accessor.
   *
   * @param accessor a state accessor containing the version's major, minor and patch.
   * @return a new SemanticVersion object
   */
  static SemanticVersion createFromStateAccessor(StateAccessor accessor) {
    return new SemanticVersion(
        accessor.get("major").cast(Integer.class),
        accessor.get("minor").cast(Integer.class),
        accessor.get("patch").cast(Integer.class));
  }
}
