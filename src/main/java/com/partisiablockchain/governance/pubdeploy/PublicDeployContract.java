package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.contract.sys.UpgradeBuilder;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.tree.AvlTree;
import java.util.Map;

/**
 * System contract for deploying public WASM contracts.
 *
 * <p><b>This contract does not support deployment of ZK/REAL WASM contracts.</b>
 *
 * <p>This is a core contract for Partisia Blockchain, as it allows any user to deploy a new smart
 * contract, using one of the stored binders. The deploy contract keeps track of multiple binders to
 * allow for a grace period before binders are fully deprecated.
 *
 * <p><a href="https://gitlab.com/partisiablockchain/language/pub-wasm-binder">Public WASM
 * Binder</a> is the primary binder that contracts will be deployed with.
 */
@AutoSysContract(PublicDeployContractState.class)
public final class PublicDeployContract {

  /** The default ID the binder used for initializing the pub-deploy contract. */
  private static final int DEFAULT_BINDER_ID = 1;

  /** Shortnames for invocations. */
  static final class Invocations {
    /** Shortname for the deprecated {@link deployContract} invocation. */
    static final int DEPLOY_CONTRACT = 1;

    /** Shortname for {@link addBinder}. */
    static final int ADD_BINDER = 2;

    /** Shortname for {@link removeBinder}. */
    static final int REMOVE_BINDER = 3;

    /** Shortname for {@link deployContractWithBinderId}. */
    static final int DEPLOY_CONTRACT_WITH_BINDER_ID = 4;

    /** Shortname for {@link upgradeContract}. */
    static final int UPGRADE_CONTRACT = 5;

    private Invocations() {}
  }

  /** Shortnames for callbacks. */
  static final class Callbacks {

    /** Shortname for {@link saveBinderCallback}. */
    static final int SAVE_BINDER_CALLBACK = 1;

    private Callbacks() {}
  }

  /**
   * Initializes the pub-deploy contract.
   *
   * @param bindingJar the public binder to use as contract binder
   * @param supportedBinderVersionMin minimum (inclusive) PUB binder version supported by the binder
   * @param supportedBinderVersionMax maximum (inclusive) PUB binder version supported by the binder
   * @param systemUpdateAddress The blockchain address of the system update contract. Only calls
   *     from this address can add and remove binders.
   * @return Initialized state with the caller as owner, and the provided binding jar
   */
  @Init
  public PublicDeployContractState create(
      byte[] bindingJar,
      SemanticVersion supportedBinderVersionMin,
      SemanticVersion supportedBinderVersionMax,
      BlockchainAddress systemUpdateAddress) {

    SupportedBinderVersionInterval binderInterval =
        new SupportedBinderVersionInterval(supportedBinderVersionMin, supportedBinderVersionMax);
    AvlTree<Integer, BinderInfo> binders =
        AvlTree.create(
            Map.of(
                DEFAULT_BINDER_ID, new BinderInfo(new LargeByteArray(bindingJar), binderInterval)));
    int nextBinderId = DEFAULT_BINDER_ID + 1;
    return new PublicDeployContractState(systemUpdateAddress, binders, nextBinderId);
  }

  /**
   * Upgrade the previous or current deploy contract state to this version.
   *
   * @param oldState the old state
   * @param systemUpdateContract The blockchain address of the system update contract. Only calls
   *     from this address can add and remove binders.
   * @return the migrated state
   */
  @Upgrade
  public PublicDeployContractState upgrade(
      StateAccessor oldState, BlockchainAddress systemUpdateContract) {
    return PublicDeployContractState.createFromStateAccessor(oldState, systemUpdateContract);
  }

  /**
   * Deploys a WASM-based smart contract on the blockchain, using the first available stored binder.
   * Creates a new address on which the contract can be interacted with.
   *
   * <p><b>This invocation is deprecated! {@link #deployContractWithBinderId} should be used
   * instead.</b>
   *
   * <p><b>Permissions</b>: Can be called by any user.
   *
   * @param context the context of the contract
   * @param state the current state of the contract
   * @param contract the jar of the contract to deploy
   * @param abi the abi of the contract
   * @param initialization any initialization rpc needed when deploying the contract
   * @return the same state
   */
  @Action(Invocations.DEPLOY_CONTRACT)
  public PublicDeployContractState deployContract(
      SysContractContext context,
      PublicDeployContractState state,
      byte[] contract,
      byte[] abi,
      byte[] initialization) {
    deployOnChain(context, state, contract, abi, initialization, DEFAULT_BINDER_ID);
    return state;
  }

  /**
   * Deploys a WASM-based smart contract on the blockchain, using the specified binder. Creates a
   * new address on which the contract can be interacted with.
   *
   * <p><b>Permissions</b>: Can be called by any user.
   *
   * @param context the context of the contract
   * @param state the current state of the contract
   * @param binderId the ID of the binder to deploy
   * @param contract the jar of the contract to deploy
   * @param abi the abi of the contract
   * @param initialization any initialization rpc needed when deploying the contract
   * @return the same state
   */
  @Action(Invocations.DEPLOY_CONTRACT_WITH_BINDER_ID)
  public PublicDeployContractState deployContractWithBinderId(
      SysContractContext context,
      PublicDeployContractState state,
      byte[] contract,
      byte[] abi,
      byte[] initialization,
      int binderId) {
    deployOnChain(context, state, contract, abi, initialization, binderId);
    return state;
  }

  /**
   * Add a new binder which can be used for deploying WASM contracts.
   *
   * <p>Saves the binder in the global governance storage and waits for the callback before saving
   * the hash to this contract's state.
   *
   * <p><b>Permissions</b>: This can only be called by the system update contract (as specified in
   * {@link PublicDeployContractState#getSystemUpdateAddress}).
   *
   * <p>The added binder can serve as a deploy target for {@link #deployContractWithBinderId}, and
   * can be removed again using {@link #removeBinder}.
   *
   * @param context the context of the contract
   * @param state the current state of the contract
   * @param bindingJar the binder JAR
   * @param supportedBinderVersionMin minimum (inclusive) PUB binder version supported by the binder
   * @param supportedBinderVersionMax maximum (inclusive) PUB binder version supported by the binder
   * @return the unchanged state
   */
  @Action(Invocations.ADD_BINDER)
  public PublicDeployContractState addBinder(
      SysContractContext context,
      PublicDeployContractState state,
      byte[] bindingJar,
      SemanticVersion supportedBinderVersionMin,
      SemanticVersion supportedBinderVersionMax) {

    // Check that invocation comes from blockchain system update contract
    ensure(
        context.getFrom().equals(state.getSystemUpdateAddress()),
        "The invocation does not come from a system update contract.");

    LargeByteArray binderData = new LargeByteArray(bindingJar);
    SystemEventManager remoteCalls = context.getRemoteCallsCreator();
    remoteCalls.registerCallbackWithCostFromRemaining(
        stream -> {
          stream.writeByte(Callbacks.SAVE_BINDER_CALLBACK);
          binderData.getIdentifier().write(stream);
          supportedBinderVersionMin.write(stream);
          supportedBinderVersionMax.write(stream);
        });
    remoteCalls.updateGlobalSharedObjectStorePluginState(
        SharedObjectStorePluginRpc.addSharedObject(bindingJar));

    return state;
  }

  /**
   * Remove a binder such that WASM contracts can no longer be deployed with it.
   *
   * <p><b>Permissions</b>: This can only be called by the system update contract (as specified in
   * {@link PublicDeployContractState#getSystemUpdateAddress}).
   *
   * @param context the context of the contract
   * @param state the current state of the contract
   * @param binderId The ID of the binder to remove
   * @return the state with the binder removed
   */
  @Action(Invocations.REMOVE_BINDER)
  public PublicDeployContractState removeBinder(
      SysContractContext context, PublicDeployContractState state, int binderId) {
    // Check that invocation comes from a system update contract
    ensure(
        context.getFrom().equals(state.getSystemUpdateAddress()),
        "The invocation does not come from a system update contract.");
    return state.removeBinder(binderId);
  }

  /**
   * Upgrade the state of an existing upgradable WASM contract on the blockchain.
   *
   * <p>Attempt to upgrade a specific contract to a new binder and contract code. The contract will
   * be consulted to check whether the upgrade is expected and allowed.
   *
   * <p><b>Permissions</b>: Can be called by any user. The permission check is delegated to the the
   * contract being upgraded.
   *
   * <p>Contract must have previously been deployed using {@link #deployContractWithBinderId}.
   *
   * @param context the context of the contract
   * @param state the current state of the contract
   * @param contract the address of the contract to upgrade
   * @param newBinderId the new binder ID
   * @param newContract the new contract
   * @param newAbi the new abi
   * @param upgradeRpc the contract upgrade rpc
   * @return the unchanged state
   */
  @Action(Invocations.UPGRADE_CONTRACT)
  public PublicDeployContractState upgradeContract(
      SysContractContext context,
      PublicDeployContractState state,
      BlockchainAddress contract,
      int newBinderId,
      byte[] newContract,
      byte[] newAbi,
      byte[] upgradeRpc) {
    byte[] newBinder = state.getBinderInfo(newBinderId).bindingJar().getData();

    UpgradeBuilder upgrade =
        context
            .getInvocationCreator()
            .upgradeContract(contract)
            .withNewBinderJar(newBinder)
            .withNewContractJar(newContract)
            .withNewAbi(newAbi)
            .withUpgradeRpc(safeDataOutputStream -> safeDataOutputStream.write(upgradeRpc));
    upgrade.send();

    return state;
  }

  /**
   * Receive callback from saving a new binder in the global governance storage plugin.
   *
   * <p>Saves the binder hash if the call was successful.
   *
   * @param context the context of the contract
   * @param state the current state of the contract
   * @param callbackContext the context of the callback
   * @param binderHash hash of the binder jar
   * @param supportedBinderVersionMin minimum (inclusive) PUB binder version supported by the binder
   * @param supportedBinderVersionMax maximum (inclusive) PUB binder version supported by the binder
   * @return the state with the binder hash added
   */
  @Callback(Callbacks.SAVE_BINDER_CALLBACK)
  public PublicDeployContractState saveBinderCallback(
      SysContractContext context,
      PublicDeployContractState state,
      CallbackContext callbackContext,
      Hash binderHash,
      SemanticVersion supportedBinderVersionMin,
      SemanticVersion supportedBinderVersionMax) {
    ensure(callbackContext.isSuccess(), "Failed to save new binder in storage plugin");
    SupportedBinderVersionInterval interval =
        new SupportedBinderVersionInterval(supportedBinderVersionMin, supportedBinderVersionMax);
    LargeByteArray wrappedBinderHash = new LargeByteArray(binderHash.getBytes());
    BinderInfo newBinderInfo = new BinderInfo(wrappedBinderHash, interval);
    return state.addBinderInfo(newBinderInfo);
  }

  private static void deployOnChain(
      SysContractContext context,
      PublicDeployContractState state,
      byte[] contract,
      byte[] abi,
      byte[] initialization,
      int binderId) {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC, context.getOriginalTransactionHash());
    byte[] binderJar = state.getBinderInfo(binderId).bindingJar().getData();
    context
        .getInvocationCreator()
        .deployContract(contractAddress)
        .withBinderJar(binderJar)
        .withContractJar(contract)
        .withAbi(abi)
        .withPayload(safeDataOutputStream -> safeDataOutputStream.write(initialization))
        .allocateRemainingCost()
        .send();
  }

  private static void ensure(boolean pred, String errorString) {
    if (!pred) {
      throw new RuntimeException(errorString);
    }
  }
}
