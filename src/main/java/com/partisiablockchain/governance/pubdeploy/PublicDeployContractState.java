package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/** State for the deployment contract for public contracts. */
@Immutable
public final class PublicDeployContractState implements StateSerializable {
  /**
   * The blockchain address of the system update contract. Only calls from this address can add and
   * remove binders.
   */
  private final BlockchainAddress systemUpdateAddress;

  /** The available binders that can be used for deploying contracts indexed by their integer ID. */
  private final AvlTree<Integer, BinderInfo> binders;

  /** The ID of the next binder ID to be used. Is incremented when a binder is added. */
  private final int nextBinderId;

  @SuppressWarnings("unused")
  PublicDeployContractState() {
    this.nextBinderId = 2;
    this.systemUpdateAddress = null;
    this.binders = null;
  }

  PublicDeployContractState(
      BlockchainAddress systemUpdateAddress, AvlTree<Integer, BinderInfo> binders, int binderId) {
    this.systemUpdateAddress = systemUpdateAddress;
    this.binders = binders;
    this.nextBinderId = binderId;
  }

  /**
   * Creates a {@link PublicDeployContractState} from an old state. Includes a system update
   * blockchain address.
   *
   * @param oldState state accessor of the old state containing an AvlTree of binders
   * @param systemUpdateAddress The blockchain address of the system update contract. Only calls
   *     from this address can add and remove binders.
   * @return a new state containing the old tree and the system update address
   */
  public static PublicDeployContractState createFromStateAccessor(
      StateAccessor oldState, BlockchainAddress systemUpdateAddress) {
    int nextBinderId = oldState.get("nextBinderId").intValue();
    StateAccessor binders = oldState.get("binders");

    AvlTree<Integer, BinderInfo> newBinders = AvlTree.create();

    for (StateAccessorAvlLeafNode binder : binders.getTreeLeaves()) {
      int id = binder.getKey().intValue();
      StateAccessor binderInfoAccessor = binder.getValue();
      BinderInfo binderInfo = BinderInfo.createFromStateAccessor(binderInfoAccessor);
      newBinders = newBinders.set(id, binderInfo);
    }
    return new PublicDeployContractState(systemUpdateAddress, newBinders, nextBinderId);
  }

  PublicDeployContractState addBinderInfo(BinderInfo binderInfo) {
    AvlTree<Integer, BinderInfo> updatedBinders = binders.set(nextBinderId, binderInfo);
    return new PublicDeployContractState(systemUpdateAddress, updatedBinders, nextBinderId + 1);
  }

  PublicDeployContractState removeBinder(int binderId) {
    AvlTree<Integer, BinderInfo> updatedBinders = this.binders.remove(binderId);
    return new PublicDeployContractState(systemUpdateAddress, updatedBinders, nextBinderId);
  }

  BlockchainAddress getSystemUpdateAddress() {
    return this.systemUpdateAddress;
  }

  BinderInfo getBinderInfo(int binderId) {
    return binders.getValue(binderId);
  }

  /**
   * Returns the integer ID of the next binder to be added.
   *
   * @return the ID of the next binder to be added
   */
  public int getNextBinderId() {
    return nextBinderId;
  }
}
