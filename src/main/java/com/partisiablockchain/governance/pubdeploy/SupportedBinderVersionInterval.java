package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Interval of supported binder versions as SemanticVersions.
 *
 * @param supportedBinderVersionMin the minimum supported binder version (inclusive)
 * @param supportedBinderVersionMax the maximum supported binder version (inclusive)
 */
@Immutable
public record SupportedBinderVersionInterval(
    SemanticVersion supportedBinderVersionMin, SemanticVersion supportedBinderVersionMax)
    implements StateSerializable {

  /**
   * Creates an instance of a SupportedBinderVersionInterval from a state accessor.
   *
   * @param accessor A state accessor containing the minimum and maximum supported binder version
   * @return a new SupportedBinderVersionInterval object
   */
  static SupportedBinderVersionInterval createFromStateAccessor(StateAccessor accessor) {
    return new SupportedBinderVersionInterval(
        SemanticVersion.createFromStateAccessor(accessor.get("supportedBinderVersionMin")),
        SemanticVersion.createFromStateAccessor(accessor.get("supportedBinderVersionMax")));
  }
}
