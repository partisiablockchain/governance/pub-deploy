package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.secata.stream.SafeDataOutputStream;

/** Build updates to the global state of the shared object store plugin. */
final class SharedObjectStorePluginRpc {

  /**
   * Invocation byte to add a new binary object to the global state of the shared object store
   * plugin.
   */
  static final int ADD_SHARED_OBJECT_INVOCATION_BYTE = 0;

  @SuppressWarnings("unused")
  private SharedObjectStorePluginRpc() {}

  /**
   * Build the update to add a new binary object to the global state of the shared object store
   * plugin.
   *
   * @param objectBytes to add to shared object store. Not nullable.
   * @return update to apply. Never null.
   */
  static GlobalPluginStateUpdate addSharedObject(byte[] objectBytes) {
    return GlobalPluginStateUpdate.create(
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(ADD_SHARED_OBJECT_INVOCATION_BYTE);
              stream.writeDynamicBytes(objectBytes);
            }));
  }
}
