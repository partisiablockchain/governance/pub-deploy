package com.partisiablockchain.governance.pubdeploy;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Binder information that can be used for deploying WASM contracts.
 *
 * <p>The information consists of either the binder jar itself, or the hash of the binder jar. The
 * information also includes the interval of binder versions the binder supports.
 *
 * @param bindingJar the binder jar or the hash of the binder jar
 * @param versionInterval the interval of binder versions that the binder JAR supports.
 */
@Immutable
public record BinderInfo(LargeByteArray bindingJar, SupportedBinderVersionInterval versionInterval)
    implements StateSerializable {

  /**
   * Creates an instance of a BinderInfo from a state accessor.
   *
   * @param accessor to the state object that holds the binder info
   * @return a new BinderInfo object
   */
  static BinderInfo createFromStateAccessor(StateAccessor accessor) {
    return new BinderInfo(
        accessor.get("bindingJar").cast(LargeByteArray.class),
        SupportedBinderVersionInterval.createFromStateAccessor(accessor.get("versionInterval")));
  }
}
